module Control where

import Data.Int
import Graphics.UI.SDL

data Action = Press | Release deriving (Show,Eq,Ord)

-- an abstract controller
data Control = 
  L | R | U | D | NLR | NUD |
  FirePress | FireRelease |
  Abort
  deriving (Show,Eq,Ord,Read)


newtype KAxisState = KAxisState ((SDLKey,Action) -> (Maybe Control, KAxisState))
mkKAxisSM :: (Control,Control,Control) -> (SDLKey,SDLKey) -> KAxisState
mkKAxisSM (cl,c0,cr) (kl,kr) = KAxisState s00 where
  goto00 = KAxisState s00
  gotol0 = KAxisState sl0
  goto0r = KAxisState s0r
  gotolr = KAxisState slr
  s00 (k,Press)   | k == kl   = (Just cl, gotol0)
                  | k == kr   = (Just cr, goto0r)
                  | otherwise = (Nothing, goto00)
  s00 (_,Release)             = (Nothing, goto00)
  sl0 (k,Press)   | k == kr   = (Just cr, gotolr)
                  | otherwise = (Nothing, gotol0)
  sl0 (k,Release) | k == kl   = (Just c0, goto00)
                  | otherwise = (Nothing, gotol0)
  s0r (k,Press)   | k == kl   = (Just cl, gotolr)
                  | otherwise = (Nothing, goto0r)
  s0r (k,Release) | k == kr   = (Just c0, goto00)
                  | otherwise = (Nothing, goto0r)
  slr (_,Press)               = (Nothing, gotolr)
  slr (k,Release) | k == kl   = (Just cr, goto0r)
                  | k == kr   = (Just cl, gotol0)
                  | otherwise = (Nothing, gotolr)
runKAxis :: (SDLKey,Action) -> KAxisState -> (Maybe Control, KAxisState)
runKAxis (k,a) (KAxisState s) = s (k,a)

newtype JAxisState = JAxisState (Int16 -> (Maybe Control, JAxisState))
mkJAxisSM :: (Control,Control,Control) -> Int16 -> JAxisState
mkJAxisSM (cl,c0,cr) thresh = JAxisState s0 where
  gotor = JAxisState sr
  gotol = JAxisState sl
  goto0 = JAxisState s0
  s0 v | v > thresh  = (Just cr, gotor)
       | v < -thresh = (Just cl, gotol)
       | otherwise   = (Nothing, goto0)
  sr v | v > thresh  = (Nothing, gotor)
       | v < -thresh = (Just cl, gotol)
       | otherwise   = (Just c0, goto0)
  sl v | v > thresh  = (Just cl, gotor)
       | v < -thresh = (Nothing, gotol)
       | otherwise   = (Just c0, goto0)
runJAxis :: Int16 -> JAxisState -> (Maybe Control, JAxisState)
runJAxis v (JAxisState s) = s v
