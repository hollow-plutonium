module Audio where

import Prelude hiding (lookup)
import Graphics.UI.SDL.Mixer
import Data.IntMap
import Control.Monad


data Audio = Audio {
  sounds :: IntMap Chunk
}

maxChan = 32

startAudio :: IO Audio
startAudio = do
  openAudio defaultFrequency AudioS16LSB 2 2048
  allocateChannels maxChan
  --load sound files
  --setup music streams
  return Audio { sounds = empty }

endAudio :: IO ()
endAudio = do
  closeAudio

whenJust :: (Monad m) => Maybe a -> (a -> m b) -> m ()
whenJust Nothing _ = return ()
whenJust (Just x) f = f x >> return ()

playSound :: Int -> Audio -> IO ()
playSound id audio = do
  let chunk = lookup id (sounds audio)
  whenJust chunk (\x -> tryPlayChannel (-1) x 0)
  return ()

fadeAll :: Audio -> IO ()
fadeAll audio = do
  fadeOutChannel (-1) 200
  return ()

