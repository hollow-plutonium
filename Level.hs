module Level where

import Prelude hiding (lookup)
import Data.Array (Array, array, (!), bounds, inRange)
import Data.IntMap (IntMap, empty, lookup, insert)
import Data.Maybe
import Data.Complex

import Image
import Coord
import Cardinal

type Board a = Array (Int,Int) a

data Level = Level {
  walls :: Board Bool,
  wallGfx :: Board Image,
  gates :: IntMap Bool
} deriving (Show)


gateKey :: (Int,Int) -> Level -> Int
gateKey (x,y) lv = x + y*w where
  w = (fst . snd) (bounds (walls lv))

setGate :: Bool -> (Int,Int) -> Level -> Level
setGate tf xy lv = lv { gates = changed } where
  changed = insert (gateKey xy lv) tf (gates lv)

raiseGate :: (Int,Int) -> Level -> Level
raiseGate = setGate True

lowerGate :: (Int,Int) -> Level -> Level
lowerGate = setGate False

{-
moveGate :: (Int,Int) -> (Int,Int) -> Level -> Level
moveGate xy xy' lv =
  if destination is available
    then move it there
    else dont
-}


walkDestination :: Cardinal -> (Int,Int) -> Level -> (Int,Int)
walkDestination dir = walk (cardPair dir)

walk :: (Int,Int) -> (Int,Int) -> Level -> (Int,Int)
walk (dx,dy) (x,y) lv =
  if oob || hitWall || hitGate
    then (x,y)
    else walk (dx,dy) next lv
  where
    next = (x+dx,y+dy) :: (Int,Int)
    arr = walls lv :: Board Bool
    k = gateKey next lv
    oob = not $ inRange (bounds arr) next
    hitWall = arr ! next :: Bool
    hitGate = fromMaybe False (lookup k (gates lv))

loadLevel :: String -> Level
loadLevel raw = let
  a = 4
  b = 3
  c = 0
  in Level {
    walls = undefined,
    wallGfx = undefined,
    gates = empty
  }


cellSize = 16
cellSquare = (cellSize:+cellSize)

whereOnGrid :: Coord -> (Int,Int)
whereOnGrid x = x00 where
  h = cellSquare/2
  cfloor (r:+i) = (floor r, floor i)
  x' = x + h
  x0 = x' / 16
  x00 = cfloor x0
