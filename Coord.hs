module Coord where

import Data.Complex

type FPNum = Double
type Coord = Complex FPNum
type Bound = (Coord,Coord)
type Velocity = Complex FPNum
type Spin = FPNum
type Radius = FPNum

overlap :: Bound -> Bound -> Bool
overlap ((x1:+y1),(w1:+h1)) ((x2:+y2),(w2:+h2)) = not miss where
  miss =
    x1 > x2+w2 ||
    x2 > x1+w1 ||
    y1 > y2+h2 ||
    y2 > y1+h1
