module Image where

import Data.Complex

data Image = Image {
  imgWH :: Complex Double,
  imgApply :: Complex Double -> IO ()
}

instance Show Image where
  show img = "Image { "++show (imgWH img)++" }"

dummyImage = Image {
  imgWH = 0,
  imgApply = \x -> return ()
}
