module Game.Test.Generic where

import Graphics.Rendering.OpenGL
import Graphics.UI.GLUT
import Engine.Entity
import Engine.Render.Test.Cube

data EntTest = EntTest {x :: GLfloat, y :: GLfloat, z :: GLfloat}

instance EntityBase EntTest where
  create = EntTest 0 0 0

instance EntitySpatial EntTest where
  xPos e = x e
  yPos e = y e
  zPos e = z e

instance EntityRender EntTest where
  render e = do
    translate (Vector3 (x e) (y e) (z e))
    cube (0.2 :: GLfloat)
    putStrLn "render hit"
    putStr "r: "
    print $ x e

instance EntityThink EntTest where
  think e w = e {x = (x e) + 0.1}

main = do
  ortho 0.0 100 0 100 (-1) 1
  let ent = create :: EntTest
  return ()
