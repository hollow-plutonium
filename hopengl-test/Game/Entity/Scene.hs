module Game.Entity.Scene where

import Control.Monad.State
import Control.Parallel.Strategies

-- Do not change! ----- v ------
type World = StateT Scene IO
type Message = World ()
-- Do not change! ----- ^ -----

-- This is your scenegraph
data Scene = Scene

updateScene :: Scene -> IO (Scene, [Message])
updateScene scene = do
  return (scene, [])
