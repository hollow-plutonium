module Test where

import qualified Game.Test.Generic as T
import Graphics.Rendering.OpenGL
import Graphics.UI.GLUT

main = do
  getArgsAndInitialize
  createWindow "Test"
  T.main
  reshapeCallback $= Just (\s -> viewport $= (Position 0 0, s))
  mainLoop
