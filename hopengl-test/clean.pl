#!/usr/bin/perl

use strict;
use File::Find;

sub clean {
  my $file = $_;
  if ($file =~ /^*\.hi$/) {
    print "Cleaning $File::Find::name\n";
    unlink $file;
  }
  if ($file =~ /^*\.o$/) {
    print "Cleaning $File::Find::name\n";
    unlink $file;
  }
}

find(\&clean, ("."));
print "Directory Cleaned\n";
