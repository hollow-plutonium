module Engine.Entity.Render where

import Engine.Entity.Base
import Engine.Entity.Spatial

class EntitySpatial e => EntityRender e where
  render :: e -> IO ()
