module Engine.Entity.Think where

import Game.Entity.Scene
import Engine.Entity.Base

class EntityBase e => EntityThink e where
  think :: e -> Scene -> e
