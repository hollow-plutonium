module Engine.Entity.World where

import Control.Monad.State
import Game.Entity.Scene

-- Runs a list of messages in order
runMessages :: [Message] -> World ()
runMessages = sequence_

runUpdate :: World ()
runUpdate = do
  scene <- get
  (scene', messages) <- lift $ updateScene scene
  runMessages messages
  put scene'
