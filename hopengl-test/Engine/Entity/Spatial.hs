module Engine.Entity.Spatial where

import Graphics.Rendering.OpenGL
import Engine.Entity.Base

class EntityBase e => EntitySpatial e where
  xPos :: e -> GLfloat
  yPos :: e -> GLfloat
  zPos :: e -> GLfloat

class EntitySpatial e => EntityRotate e where
  pitch :: e -> GLfloat
  yaw :: e -> GLfloat
  roll :: e -> GLfloat
