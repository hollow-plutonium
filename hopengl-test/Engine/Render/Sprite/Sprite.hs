module Engine.Render.Sprite.Sprite where

import Graphics.Rendering.OpenGL
import Graphics.UI.GLUT
import Control.Monad
import Texture.ReadImage

spriteInit :: Int -> Int -> IO ()
spriteInit x y = do
  blendEquation $= FuncAdd
  blend $= Enabled
  blendFunc $= (SrcAlpha, OneMinusSrcAlpha)
  textureFunction $= Replace
  texture Texture2D $= Enabled
  clearColor $= Color4 0.0 0.0 0.0 0.0
  color (Color4 0.0 0.0 0.0 (1.0 :: GLfloat))
  ortho 0.0 (fromIntegral x) 0.0 (fromIntegral y) (-1.0) (1.0)

createTexture :: FilePath -> (Bool, Bool) -> IO (Maybe TextureObject)
createTexture filename (repeatX, repeatY) = do
  [texName] <- genObjectNames 1
  textureBinding Texture2D $= Just texName
  when repeatX (textureWrapMode Texture2D S $= (Repeated, Repeat))
  when repeatY (textureWrapMode Texture2D T $= (Repeated, Repeat))
  textureFilter Texture2D $= ((Nearest, Nothing), Nearest)
  ((Size x y), pixels) <- readImage filename
  texImage2D Nothing NoProxy 0 RGBA' (TextureSize2D x y) 0 pixels
  return (Just texName)
