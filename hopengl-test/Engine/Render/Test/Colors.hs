module Engine.Render.Test.Colors where
import Graphics.Rendering.OpenGL

red    = Color3 (1.0::GLfloat) 0.0 0.0
green  = Color3 (0.0::GLfloat) 1.0 0.0
blue   = Color3 (0.0::GLfloat) 0.0 1.0
yellow = Color3 (1.0::GLfloat) 1.0 0.0
violet = Color3 (1.0::GLfloat) 1.0 0.0
teal   = Color3 (1.0::GLfloat) 0.0 1.0
white  = Color3 (1.0::GLfloat) 1.0 1.0
