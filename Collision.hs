module Collision where

data Collision =
  Null |
  Monster |
  Player Int |
  RemapControls |
  ReloadLevel |
  ModifyPlayer |
  GameOver |
  GotControlled 


