module Video (
  startVideo,
  endVideo,
  drawImage,
  Video(..),
  Backend(..)
) where

import Graphics.UI.SDL (Surface)
import Data.IntMap

import Coord
import Image
import VideoGuts

data Backend = UseSDL | UseOpenGL


startVideo :: Int -> Int -> Backend -> IO Video
startVideo w h UseSDL = do
  screen <- setupSDL w h
  return (mkClassicBackend screen)
startVideo w h UseOpenGL = do
  setupGL w h
  return mkGLBackend


mkClassicBackend :: Surface -> Video
mkClassicBackend screen = Video {
  clear = sdlClear screen,
  loadImage = sdlLoadImage screen,
  setClip = sdlSetClip screen,
  finalize = sdlFinalize screen
}

mkGLBackend :: Video
mkGLBackend = Video {
  clear = glClear,
  loadImage = glLoadImage,
  setClip = glSetClip,
  finalize = glFinalize
}

endVideo :: IO ()
endVideo = quitGuts

drawImage :: Coord -> Image -> IO ()
drawImage xy img = (imgApply img) xy

