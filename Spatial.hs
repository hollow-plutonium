module Spatial where

import Data.Set (Set)
import qualified Data.Set as Set
import Data.IntMap (IntMap)
import qualified Data.IntMap as IM
import Data.Maybe
import qualified Data.Stream as Sm
import Data.List

import Piece
import Coord
import Image
import TwoIntSet

{-
data structure to hide the details of spatial search
and collision semantics

at first it will be inefficient, hopefully later it
can be replaced with a real spatial search structure
like a quadtree (somehow)
-}


data Spatial = Spatial {
  piecemap :: IntMap Piece,
  collisions :: TwoIntSet
}

--
internalInteractions :: Spatial ->([(Piece,Piece)], [(Piece,Piece)], Spatial)
internalInteractions (Spatial pm cols) = (os, ds, Spatial pm' cols') where
  os = map f (Set.elems diff)
  ds = []
  cols' = s1
  pm' = pm
  s0 = cols
  diff = s1 Set.\\ s0
  s1 = Set.fromList intpairs
  f (Two (n,m)) = (l n, l m)
  l x = fromMaybe (error emsg) (IM.lookup x pm)
  emsg = "???"
  intpairs = map (\(p,q) -> Two (key p, key q)) overlaps
  overlaps = computeCollisions pm

moveEverything :: Spatial -> Spatial
moveEverything s = s { piecemap = m' } where
  m = piecemap s
  m' = IM.filter f . IM.map g . IM.filter h $ m
  h = isJust . absolute m
  g p = p {
    path = tail (path p),
    graphics = Sm.tail (graphics p),
    wakeup = IM.map Sm.tail (wakeup p)
  }
  f = not . null . path
    

allPieces :: Spatial -> [Piece]
allPieces s = IM.elems (piecemap s)

pokePiece :: Piece -> Collision -> Spatial -> (Spatial, [Operation])
pokePiece p c s = (s', rs) where
  m = piecemap s
  (p',rs) = (transfer p) p c
  m' =
    if null (path p')
      then IM.delete (key p') m
      else IM.insert (key p') p' m
  s' = s { piecemap = m' }

spawnPieces :: [Piece] -> Spatial -> Spatial
spawnPieces ps s = s' where
  s' = s

--


snapShotPieces :: Spatial -> [(Image, Coord)]
snapShotPieces s = catMaybes mbyImgXys where
  pcs = sort (allPieces s)
  mbyImgXys = map f pcs
  f p = absolute (piecemap s)  p >>= \xy -> return (Sm.head (graphics p), xy)



-- this will crash if there is cyclic dependency in
-- relative piece positions
absolute :: IntMap Piece -> Piece -> Maybe Coord
absolute m p = do
  x <- listToMaybe (path p)
  case anchor p of
    Nothing -> return x
    Just n -> do
      op <- IM.lookup n m
      o <- absolute m op
      return (o + x)


absoluteBound :: IntMap Piece -> Piece -> Bound
absoluteBound m p = (o + xy, wh) where
  o = fromMaybe (error emsg) (absolute m p)
  (xy,wh) = bounding p
  emsg = "absoluteBound of nowhere"


areColliding :: IntMap Piece -> Piece -> Piece -> Bool
areColliding m p1 p2 = overlap (absoluteBound m p1) (absoluteBound m p2)

computeCollisions :: IntMap Piece -> [(Piece, Piece)]
computeCollisions m =
  [
    (x,y) | 
    (x:xs) <- tails (IM.elems m),
    y <- xs,
    areColliding m x y
  ]
