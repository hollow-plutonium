module Cardinal where


data Cardinal = North | South | East | West

cardPair :: Cardinal -> (Int,Int)
cardPair North = (0,1)
cardPair South = (0,-1)
cardPair East = (1,0)
cardPair West = (-1,0)



