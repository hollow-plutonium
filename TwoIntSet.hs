module TwoIntSet where

import Data.Set

newtype Two a = Two (a,a) deriving (Show, Ord)
type TwoIntSet = Set (Two Int)
instance (Eq a) => Eq (Two a) where
  (Two x) == (Two y) = x == y || swap x == y where
    swap (n,m) = (m,n)

