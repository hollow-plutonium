module Alien where

data Alien = Alien {
  level :: Level,
  memory :: ()
}

mkAlien :: Id -> Coord -> StdGen -> Level -> Piece
mkAlien id xy rng lv = thinkAlien rng al p where
  p = Piece {
    pieceId = id,
    pieceType = Monster,
    position = repeat xy,
    graphics = repeat undefined,
    operations = empty,
    transfer = \p c -> p
  }

thinkAlien :: StdGen -> Level -> () -> Piece -> Piece
thinkAlien rng lv mem p = p { transfer = state0 } where
  state0 c = p { transfer = state1 }
  state1 c = p { transfer = state0 }
  state2 c = thinkAlien rng' lv mem (p { transfer = state' }) where
    (coin, rng') = random rng
    state' = if coin then state0 else state1
