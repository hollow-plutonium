module Player where


data Player = Player {
  fire :: Bool,
  dir :: (Int,Int),
  shots :: Int,
  lives :: Int
} deriving (Show)


setFire :: Bool -> Player -> Player
setFire tf pl = pl { fire = tf }

addDir :: (Int,Int) -> Player -> Player
addDir (dx,dy) pl = pl { dir = (x',y') } where
  (x',y') = (clamp (x+dy), clamp (y+dy))
  (x,y) = dir pl
  clamp = signum

subDir :: (Int,Int) -> Player -> Player
subDir dxdy pl = addDir (-dxdy) pl

incShots pl = pl { shots = succ (shots pl) }
decShots pl = pl { shots = pred (shots pl) }
incLives pl = pl { lives = succ (lives pl) }
decLives pl = pl { lives = pred (lives pl) }
