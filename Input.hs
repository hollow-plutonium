module Input where

import Graphics.UI.SDL
import Data.IORef
import Data.Word
import Data.Int
import Control.Monad

data Action = Press | Release deriving (Show,Ord,Eq)

data Input =
  RawKey SDLKey Action |
  RawButton Word8 Word8 Action |
  RawAxis Word8 Word8 Int16 |
  OtherRawInput
  deriving (Show, Ord, Eq)

-- return a list of all pending raw inputs
-- write True to the IORef if SDL_Quit occurs
readRawInput :: IORef Bool -> IO [Input]
readRawInput quitr = do
  z <- takeUntilM (== NoEvent) pollEvent
  when (Quit `elem` z) (writeIORef quitr True)
  return (map conv z)

conv :: Event -> Input
conv (KeyDown x) = RawKey (symKey x) Press
conv (KeyUp x) = RawKey (symKey x) Release
conv (JoyAxisMotion a b c) = RawAxis a b c
conv (JoyButtonDown a b) = RawButton a b Press
conv (JoyButtonUp a b) = RawButton a b Release
conv _ = OtherRawInput

-- make a list of results of IO actions until
-- one of the results satisfies a condition
takeUntilM :: (a -> Bool) -> IO a -> IO [a]
takeUntilM p act = do
  x <- act
  if (p x)
    then return []
    else do
      xs <- takeUntilM p act
      return (x:xs)

