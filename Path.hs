module Path where

import Data.Complex
import Data.Monoid
import Data.List

import Coord

-- a Path is a list of locations
-- it represents the discrete motion of an object starting from now
-- the [] path means the object does not exist
-- therefore a finite path manifests itself as an object that moves
-- in some way and finally disappears.
--
-- the Monoid instance allows you to compose Paths. This is a sum-type
-- composition, mappend p1 p2 is a path that follows p1 + p2 and either
-- ends when p1 or p2 ends (whichever comes first) or does not end in
-- the case p1 and p2 are infinite. the identity path is repeat 0, i.e.
-- does not move from the origin or disappear ever.
--
-- the origin. there is only one origin. in order to shift a path away
-- from the origin by amount x, mappend (repeat x).

type Path a = [a]


pempty :: Path a
pempty = []

pappend :: (Num a) => Path a -> Path a -> Path a
pappend [] _ = []
pappend _ [] = []
pappend (x:xs) (y:ys) = (x+y) : pappend xs ys

pconcat :: (Num a) => [Path a] -> Path a
pconcat xs = foldl pappend (repeat 0) xs


at :: a -> Path a
at x = repeat x

ray :: (Fractional a) => a -> Path a
ray v = map ((*) (v/100) . fromIntegral) [0..]

segment :: (Fractional a) => a -> a -> Path a
segment x 0 = []
segment x speed = []

left = segment (1:+0) 1
right = segment ((-1):+0) 1
up = segment (0:+1) 1
down = segment (0:+(-1)) 1

timeWarp :: (Fractional a) => a -> Path a -> Path a
timeWarp s ps = ps

forNext :: Int -> Path a -> Path a
forNext l ps = take l ps

(&>) :: (Num a) => Path a -> Path a -> Path a
xs &> ys = f xs ys where
  f [] _ = []
  f [x] ys = x : map (+x) ys
  f (x:xs) ys = x : f xs ys

circle :: Spin -> Radius -> Path Coord
circle 0 r = at (r:+0)
circle w r = map f xs where
  f = mkPolar r . (\x -> 2 * pi * x / steps)
  xs = [0..steps-1]
  steps = 100 / w

freeze :: Path a -> Path a
freeze (x:_) = at x


