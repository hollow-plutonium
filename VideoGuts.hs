module VideoGuts where

import Prelude hiding (init, flip)
import Graphics.UI.SDL
import Data.Complex
import Data.IntMap
import Control.Monad
import Debug.Trace

import Coord
import Image

data Video = Video {
  clear :: IO (),
  loadImage :: String -> IO Image,
  setClip :: Maybe (Coord, Coord) -> IO (),
  finalize :: IO ()
}

setupSDL :: Int -> Int -> IO Surface
setupSDL w h = do
  init [InitEverything]
  setVideoMode w h 32 []

setupGL :: Int -> Int -> IO ()
setupGL w h = do
  error "OpenGL backend non-existent"
  init [InitEverything]
  setVideoMode w h 32 [OpenGL]
  -- opengl boilerplate here
  return ()

quitGuts :: IO ()
quitGuts = quit

sdlLoadImage :: Surface -> String -> IO Image
sdlLoadImage screen path = do
  raw <- loadBMP path
  surf <- displayFormat raw
  setColorKey surf [SrcColorKey] (Pixel 0)
  return (sdlImage screen surf)

glLoadImage :: String -> IO Image
glLoadImage path = undefined

sdlSetClip :: Surface -> Maybe (Coord,Coord) -> IO ()
sdlSetClip screen Nothing = setClipRect screen Nothing
sdlSetClip screen (Just (xy,wh)) = setClipRect screen r where
  r = Just (Rect (floor x) (floor y) (floor w) (floor h))
  (x:+y) = xy
  (w:+h) = wh
glSetClip :: Maybe (Coord,Coord) -> IO ()
glSetClip = undefined

sdlClear :: Surface -> IO ()
sdlClear screen = fillRect screen Nothing (Pixel 0) >> return ()
glClear :: IO ()
glClear = error "glClear ?"

sdlFinalize :: Surface -> IO ()
sdlFinalize screen = flip screen
glFinalize :: IO ()
glFinalize = error "glFinalize ?"

loadAllImages :: Video -> IO (IntMap Image)
loadAllImages vid = return empty



glQuad = undefined
glGenImg = undefined

sdlBlit :: Surface -> Surface -> Complex Double -> IO ()
sdlBlit dst src xy = blitSurface src Nothing dst loc >> return () where
  (x :+ y) = xy
  (w,h) = (surfaceGetWidth src, surfaceGetHeight src)
  loc = Just (Rect (floor x) (floor y) w h)

loadSurface :: String -> IO Surface
loadSurface filename = loadBMP filename

{-
loadAllImages :: Video -> IO Video
loadAllImages video = do
  (numbers, files) <- return ([],[])
  images <- forM files (loadImage video)
  let table = fromList (zip numbers images)
  return (video { vidImages = table })
-}


surfWH :: Surface -> Complex Double
surfWH surf = fromIntegral (surfaceGetWidth surf) :+ fromIntegral (surfaceGetHeight surf)

sdlImage :: Surface -> Surface -> Image
sdlImage screen surf = Image {
  imgWH = surfWH surf,
  imgApply = \xy -> sdlBlit screen surf xy
}

glImage :: Int -> Rect -> Surface -> Image
glImage n rect surf = Image {
  imgWH = surfWH surf,
  imgApply = \xy -> glQuad n rect xy
}

