module Main where

import Data.Function (fix)
import Data.IORef (newIORef, readIORef, writeIORef)
import Control.Monad
import Control.Monad.State


import Clock
import Video
import Audio
import World
import Input
import Output


main :: IO ()
main = do
  video <- startVideo 320 240 UseSDL
  audio <- startAudio
  world <- (loadWorld video) >>= newIORef
  quitRef <- newIORef False
  clock <- mkClock 10  -- use 10ms time quantum (max 100fps)
  fix $ \loop -> do
    n <- readNewClockTicks clock   -- blocks until n > 0
    replicateM_ n $ do
      inputs <- readRawInput quitRef
      w <- readIORef world
      let (w',outs) = runWorld inputs w -- 1 game step
      writeIORef world w'
      forM_ outs (execOutput video audio)
    readIORef world >>= renderWorld video -- one graphics frame
    done <- readIORef quitRef
    when (not done) loop
  endAudio
  endVideo


