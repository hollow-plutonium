module Piece where

import Data.Complex
import Data.Maybe
import Data.Monoid
import Data.Stream (Stream)
import Data.IntMap (IntMap)
import Data.List
import qualified Data.Stream as Sm
import qualified Data.IntMap as IM

import Image
import Path
import Coord
import Control
import Output


data Collision =
  Overlap PieceType Piece |
  Diverge PieceType Piece |
  Controller Control |
  AI Thought

data Thought =
  XY |
  ZW |
  XZ

data Operation =
  Spawn Piece |
  GameOver |
  RemapControls |
  LoadLevel |
  Effect Output

data PieceType =
  Null |
  Monster |
  Player Int

data Piece = Piece {
  path :: Path Coord,
  graphics :: Stream Image,
  wakeup :: IntMap (Stream Thought),
  transfer :: Piece -> Collision -> (Piece, [Operation]),
  bounding :: Bound,
  self :: PieceType,
  zindex :: Int,
  key :: Int,
  anchor :: Maybe Int
}

instance Eq Piece where
  p1 == p2 = (zindex p1) == (zindex p2)

instance Ord Piece where
  p1 < p2 = (zindex p1) < (zindex p2)
  p1 <= p2 = (zindex p1) <= (zindex p2)


{-
  path :: Anchor (Path Coord),
[] means object doesnt exist and will shortly disappear before
anything else happens. otherwise, head is where he is now, head+1
is where he will be in 1 game tick, etc. relative paths are shifted
by the path of another object.

  graphics :: Stream Image,
what is drawn now, in 1 game tick, in 2, etc.

  wakeup :: IntMap (Stream Collision),
streams of collisions which apply to oneself. as time passes this
will cause you to transfer and possible spawn objects.

  transfer :: Collision -> (Piece, [Piece]),
react to a collision by changing your state and spawning zero or
more other objects. by spawning certain objects that collide with
other objects you can communicate with the system.
 
  bounding :: (Coord,Coord),
the region around oneself used to detect collisions

  self :: Collision,
what other objects see when they collide with you

  zindex :: Int
used to sort drawing
-}



newPath :: Piece -> (Path Coord -> Path Coord) -> Piece
newPath p f = p { path = f (path p) }

eliminatePiece :: Piece -> Piece
eliminatePiece p = p { path = pempty }

  

testPiece :: Image -> Coord -> Velocity -> Int -> Int -> Piece
testPiece img xy v z n = p where
  p = Piece {
    path = pconcat [at xy, ray v],
    graphics = Sm.repeat img,
    wakeup = IM.empty,
    transfer = \p _ ->
      let --p' = newPath p freeze
          --p' = eliminatePiece p
	  p' = p { path = forNext 100 (freeze (path p)) }
      in (p', []),
    bounding = (0, imgWH img),
    self = Null,
    zindex = z,
    key = n,
    anchor = Nothing
  }




