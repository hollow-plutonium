module Clock where

import Control.Monad
import Control.Concurrent
import Data.IORef
import Data.Word

import Graphics.UI.SDL.Time

data Clock = Clock {
  lastRef :: IORef Word32,
  errRef :: IORef Word32,
  quantumOf :: Word32
}

mkClock :: Int -> IO Clock
mkClock q = do
  now <- getTicks >>= newIORef
  err <- newIORef 0
  return (Clock now err (fromIntegral q))

readNewClockTicks :: Clock -> IO Int
readNewClockTicks clock@(Clock lastRef errRef quantum) = do
  now <- getTicks
  last <- readIORef lastRef
  err <- readIORef errRef
  let delta = if now >= last then now - last else maxBound - last + now
  let (steps, err') = (delta+err) `divMod` quantum
  let delay = (quantum - err') * 1000
  if steps > 0
    then do
      writeIORef lastRef now
      writeIORef errRef err'
      return (fromIntegral steps)
    else do
     -- threadDelay (fromIntegral delay)
      -- threadDelay 0
      readNewClockTicks clock
