module Output where

import Video
import Audio

data Output =
  SoundEffect Int |
  Flash |
  Quake Int

execOutput :: Video -> Audio -> Output -> IO ()
execOutput v a _ = return ()


