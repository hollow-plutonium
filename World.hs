module World where

import Prelude hiding (lookup)
import Data.IntMap (IntMap)
import qualified Data.IntMap as IM
import qualified Data.Set as Set
import Data.Complex
import Data.Maybe
import Control.Monad.State

import Piece
import Input
import Output
import Video
import Coord
import Image
import Path
import Control
import Spatial

import Debug.Trace

data World = World {
  worldSpace :: Spatial,
  camera :: Coord,
  images :: IntMap Image
}

runWorld :: [Input] -> World -> (World, [Output])
runWorld ins (World s cam img) = (World s' cam' img', outs) where
  cam' = cam
  img' = img
  (over,diver,s1) = internalInteractions s
  cols =
    concat [
      wakeUps (allPieces s),
      [],
      map (\(a,b) -> (a, Overlap (self b) b)) over,
      map (\(b,a) -> (a, Overlap (self b) b)) over,
      map (\(a,b) -> (a, Diverge (self b) b)) diver,
      map (\(b,a) -> (a, Diverge (self b) b)) diver
    ]
  f (sv,rvs) (p,c) = let (sv',r) = pokePiece p c sv in (sv',r:rvs)
  (s2,rvs) = foldl f (s1,[]) cols
  --(outs,spawns) = separateReactions (concat rvs)
  (outs,spawns) = ([],[])
  s3 = spawnPieces spawns s2
  s' = moveEverything s3



wprint :: (Show a) => a -> b
wprint x = trace (show x) undefined

wakeUps :: [Piece] -> [(Piece, Collision)]
wakeUps ps = []



renderWorld :: Video -> World -> IO ()
renderWorld video w = seeBelow where
  draws = snapShotPieces (worldSpace w)
  cxy = camera w
  seeBelow = do
    clear video
    forM_ draws (\(img,xy) -> imgApply img (xy+cxy))
    finalize video
    return ()


loadWorld :: Video -> IO World
loadWorld video = do
  img <- loadImage video "image.bmp"
  let
    pm = IM.fromList [
        (7, testPiece img (100:+0) (0:+10) 2 7),
        (5, testPiece img (100:+200) (0:+(-10)) 1 5)
      ]
  return $ World {
    worldSpace = Spatial pm (Set.empty),
    camera = 0,
    images = IM.fromList [(3,img)]
  }


